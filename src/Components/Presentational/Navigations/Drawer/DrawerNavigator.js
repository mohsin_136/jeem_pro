import 'react-native-gesture-handler'
import * as React from 'react';
import { View, Button } from "react-native";
import { createDrawerNavigator, DrawerItem } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer, DarkTheme } from '@react-navigation/native';
import HomeScreen from '../../../Containers/HomeViewController/HomeViewController'
import WashScreen from '../../../Containers/WashViewController/WashViewController'
import TypeWashcreen from '../../../Containers/TypeWashViewController/TypeWashViewController'
import PromotionScreen from '../../../Containers/PromotionsViewController/PromotionsViewController'
import PromotionDetailScreen from '../../../Containers/PromotionDetailViewController/PromotionDetailViewController'
import GetInfoScreen from '../../../Containers/GetInfoViewController/GetInfoViewController'
import ProfileScreen from '../../../Containers/ProfileVIewController/ProfileViewController'
import HistoryScreen from '../../../Containers/HistoryViewController/HistoryViewController'
import ChangePasswordScreen from '../../../Containers/ChangePasswordViewController/ChangePasswordViewController'
import ChnagePhoneNumberScreen from '../../../Containers/GetPhoneNumberViewController/GetPhoneNumberViewController'
import ContactUsScreen from '../../../Containers/ContactUsViewController/ContactUsViewController'
import Icon from 'react-native-vector-icons/Feather'
import constants from '../../../../Constants/Constants'
import { DrawerContent } from '../Drawer/DrawerContent'


const Drawerss = createDrawerNavigator();
const HomeStackNavigator = createStackNavigator();
const WashStackNavigator = createStackNavigator();
const TypeWashStackNavigator = createStackNavigator();
const PromotionsStackNavigatore = createStackNavigator();
const PromotionDetailStackNavigatore = createStackNavigator();
const GetInfoStackNavigatore = createStackNavigator();
const ProfileStackNavigator = createStackNavigator();
const HistoryStackNavigator = createStackNavigator();
const ChangePasswordStackNavigator = createStackNavigator();
const ChangePhoneNumberStackNavigator = createStackNavigator();
const ContactUsStackNavigator = createStackNavigator();


const HomeStackScreen = ({ navigation }) => (
    <HomeStackNavigator.Navigator
        screenOptions={{
            headerStyle: {
                backgroundColor: constants.themeColor,
            },
            headerTintColor: constants.whiteColor,
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerLeft: () => {
                <Icon.Button name="menu" size={30} backgroundColor="red" onPress={() => navigation.openDrawer()}>
                </Icon.Button>

            }

        }}>
        <HomeStackNavigator.Screen
            name="Home"
            component={HomeScreen}
            options={{
                title: 'Home',
                headerLeft: () => (
                    <Icon.Button name="menu" size={25} backgroundColor='#ff2241' onPress={() => navigation.openDrawer()}>
                    </Icon.Button>

                )
            }}

        />
    </HomeStackNavigator.Navigator>
);
const WashStackScreen = ({ navigation }) => (
    <WashStackNavigator.Navigator
        screenOptions={{
            headerStyle: {
                backgroundColor: constants.themeColor,
            },
            headerTintColor: constants.whiteColor,
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerLeft: () => {
                <Icon.Button name="arrow-left" size={30} backgroundColor="red" onPress={() => navigation.goBack()}>
                </Icon.Button>
            }
        }}>
        <WashStackNavigator.Screen
            name='Wash'
            component={WashScreen}
            options={{
                title: 'Wash',
                headerLeft: () => (
                    <Icon.Button name="arrow-left" size={25} backgroundColor='#ff2241' onPress={() => navigation.goBack()}>
                    </Icon.Button>

                )
            }}
        />
    </WashStackNavigator.Navigator>
);

const TypeWashStackScreen = ({ navigation }) => (
    <TypeWashStackNavigator.Navigator
        screenOptions={{
            headerStyle: {
                backgroundColor: constants.themeColor,
            },
            headerTintColor: constants.whiteColor,
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerLeft: () => {
                <Icon.Button name="arrow-left" size={30} backgroundColor="red" onPress={() => navigation.goBack()}>
                </Icon.Button>

            }
        }}>
        <TypeWashStackNavigator.Screen
            name='Type_Wash'
            component={TypeWashcreen}
            options={{
                title: 'Wash',
                headerLeft: () => (
                    <Icon.Button name="arrow-left" size={25} backgroundColor='#ff2241' onPress={() => navigation.goBack()}>
                    </Icon.Button>

                )
            }}

        />
    </TypeWashStackNavigator.Navigator>
);
const PromotionStackScreen = ({ navigation }) => (
    <PromotionsStackNavigatore.Navigator
        screenOptions={{
            headerStyle: {
                backgroundColor: constants.themeColor,
            },
            headerTintColor: constants.whiteColor,
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerLeft: () => {
                <Icon.Button name="arrow-left" size={30} backgroundColor="red" onPress={() => navigation.goBack()}>
                </Icon.Button>

            }

        }}>
        <PromotionsStackNavigatore.Screen
            name='Promotions'
            component={PromotionScreen}
            options={{
                title: 'Promotions',
                headerLeft: () => (
                    <Icon.Button name="arrow-left" size={25} backgroundColor='#ff2241' onPress={() => navigation.goBack()}>
                    </Icon.Button>

                )
            }}

        />
    </PromotionsStackNavigatore.Navigator>
);
const PromotionDetailStackScreen = ({ navigation }) => (
    <PromotionDetailStackNavigatore.Navigator
        screenOptions={{
            headerStyle: {
                backgroundColor: constants.themeColor,
            },
            headerTintColor: constants.whiteColor,
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerLeft: () => {
                <Icon.Button name="arrow-left" size={30} backgroundColor="red" onPress={() => navigation.goBack()}>
                </Icon.Button>

            }
        }}>
        <PromotionDetailStackNavigatore.Screen
            name='PromotionsDetail'
            component={PromotionDetailScreen}
            options={{
                title: 'Promotions Detail',
                headerLeft: () => (
                    <Icon.Button name="arrow-left" size={25} backgroundColor='#ff2241' onPress={() => navigation.goBack()}>
                    </Icon.Button>

                )
            }}

        />
    </PromotionDetailStackNavigatore.Navigator>
);

const GetInfoStackScreen = ({ navigation }) => (
    <GetInfoStackNavigatore.Navigator
        screenOptions={{
            headerStyle: {
                backgroundColor: constants.themeColor,
            },
            headerTintColor: constants.whiteColor,
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerLeft: () => {
                <Icon.Button name="arrow-left" size={30} backgroundColor="red" onPress={() => navigation.goBack()}>
                </Icon.Button>

            }

        }}>
        <GetInfoStackNavigatore.Screen
            name='GetInfo'
            component={GetInfoScreen}
            options={{
                title: 'Get Info',
                headerLeft: () => (
                    <Icon.Button name="arrow-left" size={25} backgroundColor='#ff2241' onPress={() => navigation.goBack()}>
                    </Icon.Button>

                )
            }}

        />
    </GetInfoStackNavigatore.Navigator>
);

const ProfileStackScreen = ({ navigation }) => (
    <ProfileStackNavigator.Navigator
        screenOptions={{
            headerStyle: {
                backgroundColor: constants.themeColor,
            },
            headerTintColor: constants.whiteColor,
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerLeft: () => {
                <Icon.Button name="menu" size={30} backgroundColor="red" onPress={() => navigation.openDrawer()}>
                </Icon.Button>

            }
        }}>
        <ProfileStackNavigator.Screen
            name="Profile"
            component={ProfileScreen}
            options={{
                title: 'Profile',
                headerLeft: () => (
                    <Icon.Button name="menu" size={25} backgroundColor='#ff2241' onPress={() => navigation.openDrawer()}>
                    </Icon.Button>

                )
            }}

        />
    </ProfileStackNavigator.Navigator>
);
const HistoryStackScreen = ({ navigation }) => (
    <HistoryStackNavigator.Navigator
        screenOptions={{
            headerStyle: {
                backgroundColor: constants.themeColor,
            },
            headerTintColor: constants.whiteColor,
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerLeft: () => {
                <Icon.Button name="menu" size={30} backgroundColor="red" onPress={() => navigation.openDrawer()}>
                </Icon.Button>

            }
        }}>
        <HistoryStackNavigator.Screen
            name="History"
            component={HistoryScreen}
            options={{
                title: 'History',
                headerLeft: () => (
                    <Icon.Button name="menu" size={25} backgroundColor='#ff2241' onPress={() => navigation.openDrawer()}>
                    </Icon.Button>

                )
            }}

        />
    </HistoryStackNavigator.Navigator>
);

const ChangePasswordStackScreen = ({ navigation }) => (
    <ChangePasswordStackNavigator.Navigator
        screenOptions={{
            headerStyle: {
                backgroundColor: constants.themeColor,
            },
            headerTintColor: constants.whiteColor,
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerLeft: () => {
                <Icon.Button name="menu" size={30} backgroundColor="red" onPress={() => navigation.openDrawer()}>
                </Icon.Button>

            }
        }}>
        <ChangePasswordStackNavigator.Screen
            name="ChangePassword"
            component={ChangePasswordScreen}
            options={{
                title: 'Change Password',
                headerLeft: () => (
                    <Icon.Button name="menu" size={25} backgroundColor='#ff2241' onPress={() => navigation.openDrawer()}>
                    </Icon.Button>

                )
            }}

        />
    </ChangePasswordStackNavigator.Navigator>
);

const ChnagePhoneNumberStackScreen = ({ navigation }) => (
    <ChangePhoneNumberStackNavigator.Navigator
        screenOptions={{
            headerStyle: {
                backgroundColor: constants.themeColor,
            },
            headerTintColor: constants.whiteColor,
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerLeft: () => {
                <Icon.Button name="menu" size={30} backgroundColor="red" onPress={() => navigation.openDrawer()}>
                </Icon.Button>

            }
        }}>
        <ChangePhoneNumberStackNavigator.Screen
            name="ChangePhonenumber"
            component={ChnagePhoneNumberScreen}
            options={{
                title: 'Change Phone Number',
                headerLeft: () => (
                    <Icon.Button name="menu" size={25} backgroundColor='#ff2241' onPress={() => navigation.openDrawer()}>
                    </Icon.Button>

                )
            }}

        />
    </ChangePhoneNumberStackNavigator.Navigator>
);
const ContactUsStackScreen = ({ navigation }) => (
    <ContactUsStackNavigator.Navigator
        screenOptions={{
            headerStyle: {
                backgroundColor: constants.themeColor,
            },
            headerTintColor: constants.whiteColor,
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerLeft: () => {
                <Icon.Button name="arrow-left" size={30} backgroundColor="red" onPress={() => navigation.goBack()}>
                </Icon.Button>

            }
        }}>
        <ContactUsStackNavigator.Screen
            name='ContactUs'
            component={ContactUsScreen}
            options={{
                title: 'ContactUs',
                headerLeft: () => (
                    <Icon.Button name="arrow-left" size={25} backgroundColor='#ff2241' onPress={() => navigation.goBack()}>
                    </Icon.Button>

                )
            }}

        />
    </ContactUsStackNavigator.Navigator>
);

const DrawerNavigation = () => {
    return (
        <Drawerss.Navigator drawerContent={props => <DrawerContent {...props} />}>
            <Drawerss.Screen name="Home" component={HomeStackScreen} />
            <Drawerss.Screen name="Wash" component={WashStackScreen} />
            <Drawerss.Screen name="Type_Wash" component={TypeWashStackScreen} />
            <Drawerss.Screen name="Promotions" component={PromotionStackScreen} />
            <Drawerss.Screen name="PromotionsDetail" component={PromotionDetailStackScreen} />
            <Drawerss.Screen name="GetInfo" component={GetInfoStackScreen} />
            <Drawerss.Screen name="Profile" component={ProfileStackScreen} />
            <Drawerss.Screen name="History" component={HistoryStackScreen} />
            <Drawerss.Screen name="ChangePassword" component={ChangePasswordStackScreen} />
            <Drawerss.Screen name="ChangePhonenumber" component={ChnagePhoneNumberStackScreen} />
            <Drawerss.Screen name="ContactUs" component={ContactUsStackScreen} />
        </Drawerss.Navigator>
    );
}
export default DrawerNavigation
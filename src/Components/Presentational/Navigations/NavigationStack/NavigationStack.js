import 'react-native-gesture-handler'
import React, { useEffect } from 'react';
import { View, ActivityIndicator } from "react-native";
import {
    NavigationContainer,
} from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import LoginViewController from '../../../Containers/LoginViewController/LoginViewController'
import HomeViewController from '../../../Containers/HomeViewController/HomeViewController'
import PormotionDetail from '../../../Containers/PromotionDetailViewController/PromotionDetailViewController'
import SignUpViewController from '../../../Containers/SignUpViewController/SignUpViewController'
import GetPhoneNumberViewController from '../../../Containers/GetPhoneNumberViewController/GetPhoneNumberViewController'
import OTPViewController from '../../../Containers/OTPViewController/OTPViewController'
import DrawerNavigation from '../Drawer/DrawerNavigator'
import AsyncStorage from '@react-native-community/async-storage'

import PromotionDetailViewController from '../../../Containers/PromotionDetailViewController/PromotionDetailViewController';
const Stack = createStackNavigator();
const NavigationStack = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator >
                <Stack.Screen
                    name="Login"
                    component={LoginViewController}
                    options={
                        {
                            headerShown: false
                        }
                    }
                />
                <Stack.Screen
                    name="SignUp"
                    component={SignUpViewController}
                    options={
                        {
                            headerShown: false
                        }
                    }
                />

                <Stack.Screen
                    name="Home"
                    component={DrawerNavigation}
                    options={
                        {
                            headerShown: false
                        }
                    }

                />
                <Stack.Screen
                    name="OTP"
                    component={OTPViewController}
                    options={
                        {
                            headerShown: false
                        }
                    }

                />
                <Stack.Screen
                    name="VarifyPhone"
                    component={GetPhoneNumberViewController}
                    options={
                        {
                            headerShown: false
                        }
                    }

                />

<Stack.Screen
                    name="PromotionsDetail"
                    component={PromotionDetailViewController}
                    options={
                        {
                            headerShown: false
                        }
                    }
                />

            




    </Stack.Navigator>
        </NavigationContainer>
    )
}
export default NavigationStack
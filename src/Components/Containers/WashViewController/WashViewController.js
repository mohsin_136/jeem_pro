import React, { Component } from 'react';
import { Text, View, FlatList, TouchableOpacity, StatusBar, StyleSheet, Dimensions, Image, ScrollView } from 'react-native'
import { Badge, Title, Paragraph, Switch } from 'react-native-paper'
import LinearGradient from 'react-native-linear-gradient'
const windowWidth = Dimensions.get('window').width;
import styles from '../../../Styles/styles'
import constants from '../../../Constants/Constants'

const WashViewController =  ({ navigation }) =>  {
    const [data, setData] = React.useState({
        dataSource: [
            {
                key: '1',
                name: 'CAR WASH',
                image: "https://www.mjcarwash.com/wp-content/uploads/2013/08/slider11.jpg",
                discription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
            },
            {
                key: '2',
                name: 'BIKE WASH',
                image: "https://i.ytimg.com/vi/q_wrGTfdAUQ/maxresdefault.jpg",
                discription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
            },
            {
                key: '3',
                name: 'TRUCK WASH',
                
                image:"https://www.detailxpertsfranchise.com/wp-content/uploads/2018/04/truck-wash.jpg",
                discription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
            },

        ],

    });
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
            <StatusBar backgroundColor="#ff2241" barStyle="light-content" />

            <FlatList
                showsVerticalScrollIndicator={false}
                style={{ marginTop: 40 }}
                data={data.dataSource}
                renderItem={({ item }) => (
            <TouchableOpacity
            onPress={() => {
                navigation.navigate("Type_Wash")
              }}
                >
            <View style={{ justifyContent: 'flex-end', alignItems: 'center', backgroundColor: '#FFFFFF50', height: 400, width: windowWidth - 30, marginTop: 30, marginBottom: 30}}>
                        <Image style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            bottom: 0,
                            right: 0,
                            width: '100%',
                            height: 200,
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10
                        }}
                            resizeMode='stretch'
                            source={{ uri: item.image }}
                        />
                        <View style={{ backgroundColor: 'white', width: '100%', borderRadius: 10, height: 200 }}>
                            <Title style={{ paddingTop: 5, paddingLeft: 15, fontFamily:'Montserrat-Bold' }}> {item.name} </Title>
                            <Title style={{ paddingTop: 3, fontSize: 12, paddingLeft: 15, fontFamily:"Montserrat-SemiBold" }}> About this package </Title>
                            <Paragraph style={{ paddingLeft: 15, fontSize: 10, fontFamily:"Montserrat-Regular" }}> {item.discription} </Paragraph>
                        </View>
                    </View>

                    </TouchableOpacity>
                )}
            />

        </View>
    )
}

export default WashViewController
import React, {useState} from 'react';
import { Text, View, TextInput, Button, Linking, TouchableOpacity, StatusBar, ScrollView, Image } from 'react-native'
import * as Animatable from 'react-native-animatable'
import LinearGradient from 'react-native-linear-gradient'
import styles from '../../../Styles/styles'
import constants from '../../../Constants/Constants'
import Entypo from 'react-native-vector-icons/Entypo'

const ContactUsViewController = ({ navigation }) => {
   
    const openFbPage = () => {
        Linking.canOpenURL("fb://profile/weiredmonivic007").then(supported => {
            if (supported) {
              return Linking.openURL("fb://profile/weiredmonivic007");
            } else {
              return Linking.openURL("https://www.facebook.com/");
            }
          })
    }
    const openInstagramPage = () => {
        Linking.canOpenURL("instagram://profile/weiredmonivic007").then(supported => {
            if (supported) {
              return Linking.openURL("instagram://profile/weiredmonivic007");
            } else {
              return Linking.openURL("https://www.instagram.com/");
            }
          })
    }
    const openTwitterPage = () => {
        Linking.canOpenURL("twitter://profile/weiredmonivic007").then(supported => {
            if (supported) {
              return Linking.openURL("twitter://profile/weiredmonivic007");
            } else {
              return Linking.openURL("https://www.twitter.com/");
            }
          })
    }

    const openMail = () => {
        Linking.canOpenURL('mailto:support@example.com').then(supported => {
            if (supported) {
              return Linking.openURL('mailto:support@example.com');
            } else {
              return Linking.openURL("https://www.mail.google.com/");
            }
          })
    }

       return(
            <View style={styles.loginScreenContainer}>
                <StatusBar backgroundColor="#ff2241" barStyle="light-content" />
                <View style={styles.loginScreenHeader}>
                    <Image style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        bottom: 0,
                        right: 0,
                        width: '100%',
                        height: 200
                    }}
                        resizeMode='stretch'
                        source={require('../../../Assets/Jeem.jpg')} />
                </View>
                <Animatable.View animation="fadeInUpBig" style={styles.loginScreenFooter}>
             
                <TouchableOpacity style={{flexDirection:'row', marginTop:50}} onPress={()=> openMail()}>
                  <Entypo name={"mail-with-circle"} size={35} />
                  <Text style={{fontFamily:"Montserrat-SemiBold", marginTop:7, marginLeft:5}}> mohsin@softcircles.com </Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={{flexDirection:'row', marginTop:30}} onPress={()=> openFbPage()}>
                  <Entypo name={"facebook-with-circle"} size={35} />
                  <Text style={{fontFamily:"Montserrat-SemiBold", marginTop:7, marginLeft:5}}> facebook.com/weiredmonivic007/ </Text>
                  </TouchableOpacity>
                 
                  <TouchableOpacity onPress={()=> openInstagramPage()} style={{flexDirection:'row',  marginTop:30}}>
                  <Entypo name={"instagram-with-circle"} size={35} />
                  <Text style={{fontFamily:"Montserrat-SemiBold", marginTop:7, marginLeft:5}}> instagram.com/weiredmonivic007/ </Text>
                  </TouchableOpacity>
                 
                  
                  <TouchableOpacity onPress={()=> openTwitterPage() } style={{flexDirection:'row',  marginTop:30}}>
                  <Entypo name={"twitter-with-circle"} size={35} />
                  <Text style={{fontFamily:"Montserrat-SemiBold", marginTop:7, marginLeft:5}}> twitter.com/weiredmonivic007/ </Text>
                  </TouchableOpacity>
                   
         
                        
                </Animatable.View>
    
    
            </View>
           )
}
export default ContactUsViewController
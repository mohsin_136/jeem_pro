import React, { Component } from 'react';
import { Text, View, FlatList, TouchableOpacity, StatusBar, StyleSheet, Dimensions, Image, ScrollView } from 'react-native'
import { Badge, Title, Paragraph, Switch } from 'react-native-paper'
import LinearGradient from 'react-native-linear-gradient'
import Icon from 'react-native-vector-icons/Feather'
const windowWidth = Dimensions.get('window').width;
import styles from '../../../Styles/styles'
import constants from '../../../Constants/Constants'

const PromotionDetailViewController = ({ route, navigation }) => {
    const configureAdditionalCustoizations = (item) => {
        var customizationViews = []
        item.forEach(element => {
            customizationViews.push(
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: 20 }}>
                    <Switch style={{ marginLeft: 12, height: 20 }} value={element.on} />
                    <Text style={{ color: 'white', marginLeft: 5, marginTop: 5 }}> {element.title} </Text>


                </View>
            )
        });
        return customizationViews
    }

    const buyPackage = () => {
         navigation.navigate("GetInfo")
        //navigation.pop()
    }


    return (
        <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
            <StatusBar backgroundColor="#ff2241" barStyle="light-content" />
            <View style={{ height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: "#ff2241", position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }}>
                <Icon.Button name="arrow-left" size={25} backgroundColor='#ff2241' onPress={() => navigation.goBack()}>
                </Icon.Button>
                <Text style={{ fontSize: 20, color: 'white', marginLeft: 40 }}>
                    {route.params.itemName}
                </Text>
            </View>
            <View style={{ justifyContent: 'flex-end', alignItems: 'center', backgroundColor: '#FFFFFF50', height: 620, width: windowWidth , marginTop: 50, marginBottom: 30 }}>

                <Image style={{
                    position: 'absolute',
                    top: 10,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    width: '100%',
                    height: 200,
                    borderTopLeftRadius: 10,
                    borderTopRightRadius: 10
                }}
                    resizeMode='stretch'
                    source={{ uri: route.params.itemImage }}
                />

                <View style={{ backgroundColor: 'white', width: windowWidth , borderRadius: 10, height: 400 }}>

                    <Title style={{ paddingTop: 5, paddingLeft: 15 }}> {route.params.itemName} </Title>
                    <Title style={{ paddingTop: 3, fontSize: 12, paddingLeft: 15 }}> About this package </Title>
                    <Paragraph style={{ paddingLeft: 15, fontSize: 10 }}> {route.params.itemDes} </Paragraph>

                    <Title style={{ paddingTop: 3, fontSize: 12, paddingLeft: 15 }}> Additional Customizations </Title>


                    <ScrollView nestedScrollEnabled={true} style={{ backgroundColor: 'black', marginLeft:10, marginRight:10 }} >
                        {configureAdditionalCustoizations(route.params.itemCustomizations)}
                    </ScrollView>

                    <TouchableOpacity style={{marginTop:15}} onPress={() => { buyPackage() }
                    }>
                        <View style={styles.pkgBtn}>
                            <LinearGradient
                                colors={[constants.themeColor, constants.themeColor]}
                                style={styles.btnBuyPakage}
                            >
                                <Text style={styles.btnTextBuyPakage}>
                                    Buy Package
         </Text>


                            </LinearGradient>
                        </View>

                    </TouchableOpacity>
                </View>


            </View>

        </View>
    )
}

export default PromotionDetailViewController
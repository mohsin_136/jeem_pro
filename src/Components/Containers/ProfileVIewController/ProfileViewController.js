import * as React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Dimensions
} from 'react-native';
import * as Animatable from 'react-native-animatable'
import LinearGradient from 'react-native-linear-gradient'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Feather from 'react-native-vector-icons/Feather'
import Material from 'react-native-vector-icons/MaterialCommunityIcons'
import PhoneInput from 'react-native-phone-number-input';
import ImagePicker from 'react-native-image-picker';
import constants from '../../../Constants/Constants'
import styless from '../../../Styles/styles'
const windowWidth = Dimensions.get('window').width;

const ProfileViewController = ({navigation}) => {
    const [data, setData] = React.useState({
        userName: 'VIC',
        email: 'vic@gmai;.com',
        password: '123',
        phoneNumber: '03384575845',
        secureTextEntry: true,
        editProfile: false,
        imagePath: {},
        imagrURI:''
    });
   const chooseFile = () => {
        var options = {
          title: 'Select Image',
          customButtons: [
            { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
          ],
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
        ImagePicker.showImagePicker(options, response => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            alert(response.customButton);
          } else {
            let source = response;
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
           setData({
               ...data,
               imagePath: source,
               imagrURI: source.uri
           })
          }
        });
      };
    const handlePhoneNumberInput = (text) => {
        setData({
            ...data,
            phoneNumber: text

        })
    }
    const handleUsernameInputChange = (val) => {
            setData({
                ...data,
                userName: val,
            })

    }
    const handleEmailInputChange = (val) => {
    
            setData({
                ...data,
                email: val,
               
            })

        
      
    }
    const handlePasswordChange = (val) => {

        setData({
            ...data,
            password: val
        })
    }
    const updateSecureTextEntry = (val) => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        })
    }
    const edit = () => {
       console.log("Edit")
       setData({
        ...data,
        editProfile: true
    })
    }
    const save = () => {
        
        setData({
         ...data,
         editProfile: false
     })
     }
  
    const renderFileUri =() =>{
        if (data.imagrURI) {
          return <Image
            source={{ uri: data.imagrURI }}
            style={styles.avatar}
          />
        } else {
          return <Image
           style={styles.avatar} source={{uri: 'https://bootdey.com/img/Content/avatar/avatar6.png'}}
          
          />
        }
    }

    return(
        <View style={styles.container}>
            <ScrollView>
        <View style={styles.header}>
        <Image style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    width: '100%',
                    height: 200

                }}
                    resizeMode='stretch'

                    source={require('../../../Assets/Jeem.jpg')} />
        </View>
       {renderFileUri()}
        {data.editProfile ? 
           <TouchableOpacity onPress = {()=> chooseFile()} style={{
                            position: 'absolute',
                            left: windowWidth - (windowWidth/2 - 45) ,
                            top: 150,
                           
                        }}  >
                            <FontAwesome
                            name="edit"
                            color="#ffff"
                            size={20}
                        />
                        </TouchableOpacity>
: null}
        <View style={styles.body}>
          <View style={styles.bodyContent}>
         
                    <Text style={styless.text_footer}>
                        User Name
    </Text>

                    <View style={styless.action}>
                        <FontAwesome
                            name="user-o"
                            color="#05375a"
                            size={20}
                        />
                        <TextInput
                            placeholder='Username'
                            style={styless.textInput}
                            autoCapitalize='none'
                            defaultValue = {data.userName}
                            editable = {data.editProfile}
                            onChangeText={(val) => handleUsernameInputChange(val)}
                        />
              
          </View>
          <Text style={[styless.text_footer, { marginTop: 35 }]}>
                        Email
    </Text>

                    <View style={styless.action}>
                        <Material
                            name="email-outline"
                            color="#05375a"
                            size={20}
                        />

                        <TextInput
                            placeholder='Email'
                            style={styless.textInput}
                            autoCapitalize='none'
                            defaultValue = {data.email}
                            editable = {data.editProfile}
                            onChangeText={(val) => handleEmailInputChange(val)}
                        />
                    </View>



{ data.editProfile ?
   <TouchableOpacity onPress={() => { save() }
}>
    <View style={styless.button}>
        <LinearGradient
            colors={[constants.themeColor, constants.themeColor]}
            style={styless.btnSignIn}
        >
            <Text style={styless.btnText}>
                Save Profile
</Text>


        </LinearGradient>
    </View>

</TouchableOpacity>
:
                    <TouchableOpacity onPress={() => { edit() }
                    }>
                        <View style={styless.button}>
                            <LinearGradient
                                colors={[constants.themeColor, constants.themeColor]}
                                style={styless.btnSignIn}
                            >
                                <Text style={styless.btnText}>
                                    Edit Profile
         </Text>


                            </LinearGradient>
                        </View>

                    </TouchableOpacity>
}

          
      </View>
      </View>
      </ScrollView>
    </View>
    ) 
 }
 const styles = StyleSheet.create({
    header:{
      backgroundColor: "#00BFFF",
      height:200,
    },
    avatar: {
      width: 130,
      height: 130,
      borderRadius: 63,
      borderWidth: 4,
      borderColor: "white",
      marginBottom:10,
      alignSelf:'center',
      position: 'absolute',
      marginTop:130
    },
    name:{
      fontSize:22,
      color:"#FFFFFF",
      fontWeight:'600',
    },
    body:{
      marginTop:40,
    },
    bodyContent: {
      flex: 1,
     // alignItems: 'center',
      padding:30,
    },
    name:{
      fontSize:28,
      color: "#696969",
      fontWeight: "600"
    },
    info:{
      fontSize:16,
      color: "#00BFFF",
      marginTop:10
    },
    description:{
      fontSize:16,
      color: "#696969",
      marginTop:10,
      textAlign: 'center'
    },
    buttonContainer: {
      marginTop:10,
      height:45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:250,
      borderRadius:30,
      backgroundColor: "#00BFFF",
    },
  })
export default ProfileViewController
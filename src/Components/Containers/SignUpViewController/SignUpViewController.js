import * as React from 'react';
import { Text, View, TextInput, Button, TouchableOpacity, StatusBar, ScrollView, Image } from 'react-native'
import * as Animatable from 'react-native-animatable'
import LinearGradient from 'react-native-linear-gradient'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Feather from 'react-native-vector-icons/Feather'
import Material from 'react-native-vector-icons/MaterialCommunityIcons'
import PhoneInput from 'react-native-phone-number-input';

import styles from '../../../Styles/styles'
import constants from '../../../Constants/Constants'
const SignUpViewController = ({ navigation }) => {
    const [data, setData] = React.useState({
        userName: '',
        email: '',
        password: '',
        phoneNumber: '',
        check_textInputChange: false,
        secureTextEntry: true
    });
    const handlePhoneNumberInput = (text) => {
        setData({
            ...data,
            phoneNumber: text

        })
    }
    const handleUsernameInputChange = (val) => {
        if (val.length !== 0) {
            setData({
                ...data,
                userName: val,
                check_textInputChange: true
            })

        }
        else {
            setData({
                ...data,
                userName: val,
                check_textInputChange: false
            })
        }
    }
    const handleEmailInputChange = (val) => {
        if (val.length !== 0) {
            setData({
                ...data,
                email: val,
                check_textInputChange: true
            })

        }
        else {
            setData({
                ...data,
                email: val,
                check_textInputChange: false
            })
        }
    }
    const handlePasswordChange = (val) => {

        setData({
            ...data,
            password: val
        })
    }
    const updateSecureTextEntry = (val) => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        })
    }
    const loginHandle = () => {
        navigation.navigate('Login')
    }
    const signUp = () => {
        console.log(data.email + " " + data.userName + " " + data.password)

    }

    return (
        <View style={styles.loginScreenContainer}>
            <StatusBar backgroundColor="#ff2241" barStyle="light-content" />
            <View style={styles.loginScreenHeader}>
                <Image style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    width: '100%',
                    height: 200

                }}
                    resizeMode='stretch'

                    source={require('../../../Assets/Jeem.jpg')} />
            </View>
            <Animatable.View animation="fadeInUpBig" style={styles.loginScreenFooter}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text style={styles.text_footer}>
                        User Name
    </Text>

                    <View style={styles.action}>
                        <FontAwesome
                            name="user-o"
                            color="#05375a"
                            size={20}
                        />
                        <TextInput
                            placeholder='Username'
                            style={styles.textInput}
                            autoCapitalize='none'
                            onChangeText={(val) => handleUsernameInputChange(val)}
                        />
                        {
                            data.check_textInputChange ?
                                <Animatable.View

                                    animation="bounceIn"
                                >
                                    <Feather
                                        name="check-circle"
                                        color='green'
                                        size={20}
                                    />
                                </Animatable.View>
                                : null
                        }
                    </View>

                    <Text style={[styles.text_footer, { marginTop: 35 }]}>
                        Email
    </Text>

                    <View style={styles.action}>
                        <Material
                            name="email-outline"
                            color="#05375a"
                            size={20}
                        />

                        <TextInput
                            placeholder='Email'
                            style={styles.textInput}
                            autoCapitalize='none'
                            onChangeText={(val) => handleEmailInputChange(val)}
                        />
                    </View>

                    <Text style={[styles.text_footer, { marginTop: 35 }]}>
                        Phone
    </Text>
                    <View style={styles.action}>
                        <PhoneInput
                            containerStyle={{ height: 50, backgroundColor: 'white' }}
                            textInputStyle={{ backgroundColor: 'white', height: 50 }}
                            codeTextStyle={{ backgroundColor: 'white' }}
                            flagButtonStyle={{ height: 50 }}
                            defaultValue={data.phoneNumber}
                            defaultCode='SA'
                            onChangeText={(text) => handlePhoneNumberInput(text)}
                        />

                    </View>

                    <Text style={[styles.text_footer, { marginTop: 35 }]}>
                        Password
    </Text>
                    <View style={styles.action}>
                        <Feather
                            name="lock"
                            color="#05375a"
                            size={20}
                        />
                        <TextInput
                            placeholder='Password'
                            style={styles.textInput}
                            autoCapitalize='none'
                            secureTextEntry={data.secureTextEntry ? true : false}
                            onChangeText={(val) => handlePasswordChange(val)}
                        />
                        <TouchableOpacity onPress={updateSecureTextEntry}>
                            {data.secureTextEntry ?
                                <Feather
                                    name="eye-off"
                                    color='grey'
                                    size={20}
                                />
                                :
                                <Feather
                                    name="eye"
                                    color='grey'
                                    size={20}
                                />
                            }
                        </TouchableOpacity>
                    </View>



                    <TouchableOpacity onPress={() => { signUp() }
                    }>
                        <View style={styles.button}>
                            <LinearGradient
                                colors={[constants.themeColor, constants.themeColor]}
                                style={styles.btnSignIn}
                            >
                                <Text style={styles.btnText}>
                                    Sign Up
         </Text>


                            </LinearGradient>
                        </View>

                    </TouchableOpacity>

                    <TouchableOpacity style={{ marginBottom: 30 }} onPress={() => { loginHandle() }
                    }>
                        <View style={styles.button}>

                            <Text style={{ color: 'black' }}>
                                Already have an account? Sing In
         </Text>



                        </View>

                    </TouchableOpacity>

                </ScrollView>

            </Animatable.View>


        </View>
    )

}
export default SignUpViewController
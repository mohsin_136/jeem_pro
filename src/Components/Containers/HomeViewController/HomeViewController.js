import React, { Component, useState } from 'react';
import { Text, View, TextInput, TouchableOpacity, StatusBar, StyleSheet, Dimensions, Image, ScrollView } from 'react-native'
import * as Animatable from 'react-native-animatable'
import { Badge, Title, Paragraph } from 'react-native-paper'
import { SliderBox } from "react-native-image-slider-box";
const windowWidth = Dimensions.get('window').width;


const HomeViewController = ({ navigation }) => {
    const [data, setData] = useState({
        images: [
            "https://source.unsplash.com/1024x768/?nature",
            "https://source.unsplash.com/1024x768/?water",
            "https://source.unsplash.com/1024x768/?girl",
            "https://source.unsplash.com/1024x768/?tree",
            require('../../../Assets/promo.jpg'),
        ]
    })
    
    React.useEffect(() => {
        return fetch('Your URL to fetch data from')
        .then((response) => response.json())
        .then((responseJson) => {
          return responseJson.movies;
        })
        .catch((error) => {
          console.error(error);
        });
      }, []);

   
    const sliderView = () => {
        var slides = []
        for (let i = 0; i < 5; i++) {

            slides.push(
                <View key={i} style={{ height: 120 }}>
                    <Image style={{
                        width: windowWidth - 20,
                        height: 200

                    }}
                        resizeMode='stretch'

                        source={require('../../../Assets/promo.jpg')} />
                </View>
            )
            if (i === 4) {
                return slides
            }
        }
    }

    const promotionPressed = (index) => {
        navigation.navigate("GetInfo")
    }

    return (
        <ScrollView>
            <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column', marginBottom: 15}}>
                <StatusBar backgroundColor="#ff2241" barStyle="light-content" />

                <View style={{ height: 200, top: 10 }}>
                    <SliderBox

                        images={data.images}
                        onCurrentImagePressed={index => { promotionPressed(index) }}
                        dotColor="#FFEE58"
                        inactiveDotColor="#90A4AE"
                        paginationBoxVerticalPadding={20}
                        autoplay
                        circleLoop
                        resizeMethod={'resize'}
                        resizeMode={'cover'}
                        paginationBoxStyle={{
                            position: "absolute",
                            bottom: 0,
                            padding: 0,
                            alignItems: "center",
                            alignSelf: "center",
                            justifyContent: "center",
                            paddingVertical: 10
                        }}
                        dotStyle={{
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            marginHorizontal: 0,
                            padding: 0,
                            margin: 0,
                            backgroundColor: "rgba(128, 128, 128, 0.92)"
                        }}
                        ImageComponentStyle={{ borderRadius: 15, width: '97%', marginTop: 5 }}
                        imageLoadingColor="#2196F3"
                    />
                </View>
                {/* <Badge style={{
                    position: "absolute",
                    transform: [{ rotate: "-15deg" }],
                    right: 10,
                    bottom: -5,
                    zIndex: 2,
                }}
                    size={20}
                >
                    100% Discount
            </Badge> */}
                <TouchableOpacity onPress={() => { navigation.navigate("Wash") }}>
                    <View style={{ justifyContent: 'flex-end', alignItems: 'center', backgroundColor: '#FFFFFF50', height: 300, width: windowWidth - 30, marginTop: 30, }}>

                        <Image style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            bottom: 0,
                            right: 0,
                            width: '100%',
                            height: 200,
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10


                        }}
                            resizeMode='stretch'

                            source={require('../../../Assets/Jeem.jpg')} />



                        <View style={{ backgroundColor: 'white', height: 100, width: '100%', borderRadius: 10 }}>
                            <Title style={{ paddingTop: 10, paddingLeft: 15 }}> WASH </Title>
                            <Paragraph style={{ paddingLeft: 15 }}> Lorem Ipsum is simply dummy text of the printing and typesetting industry. </Paragraph>
                        </View>
                    </View>
                </TouchableOpacity>

                {/* <Badge style={{
                    position: "absolute",
                    transform: [{ rotate: "-15deg" }],
                    right: 10,
                    bottom: -5,
                    zIndex: 2,
                }}
                    size={20}
                >
                    100% Discount
            </Badge> */}
                <TouchableOpacity style={{ marginTop: 20 }} onPress={() => { navigation.navigate("Wash") }}>
                    <View style={{ justifyContent: 'flex-end', alignItems: 'center', backgroundColor: '#FFFFFF50', height: 300, width: windowWidth - 30, marginTop: 30, }}>

                        <Image style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            bottom: 0,
                            right: 0,
                            width: '100%',
                            height: 200,
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10


                        }}
                            resizeMode='stretch'

                            source={require('../../../Assets/Jeem.jpg')} />

                        <View style={{ backgroundColor: 'white', height: 100, width: '100%', borderRadius: 10 }}>
                            <Title style={{ paddingTop: 10, paddingLeft: 15 }}> Detail </Title>
                            <Paragraph style={{ paddingLeft: 15 }}> Lorem Ipsum is simply dummy text of the printing and typesetting industry. </Paragraph>
                        </View>
                    </View>
                </TouchableOpacity>

            </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    viewBox: {
        paddingHorizontal: 20,
        justifyContent: 'center',
        width: windowWidth,
        padding: 10,
        alignItems: 'center',
        height: 120,

    },
    slider: {

        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'pink',
        width: windowWidth - 20,
        marginTop: 20,
        borderRadius: 10

    },
    dotContainer: {
        backgroundColor: 'transparent',
        position: 'absolute',
        bottom: 0
    }
});

export default HomeViewController
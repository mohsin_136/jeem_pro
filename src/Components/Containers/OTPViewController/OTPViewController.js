import React, {useState} from 'react';
import { Text, View, TextInput, Button, TouchableOpacity, StatusBar, ScrollView, Image } from 'react-native'
import * as Animatable from 'react-native-animatable'
import LinearGradient from 'react-native-linear-gradient'
import styles from '../../../Styles/styles'
import constants from '../../../Constants/Constants'

const OTPViewController = ({ navigation }) => {
    const [otp, setOtp] = useState(['-', '-', '-', '-', '-', '-']);
    const [otpVal, setOtpVal] = useState('');

       const submit =() => {
           console.log(otp)
           navigation.navigate("Home")
       }

       return(
            <View style={styles.loginScreenContainer}>
                <StatusBar backgroundColor="#ff2241" barStyle="light-content" />
                <View style={styles.loginScreenHeader}>
                    <Image style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        bottom: 0,
                        right: 0,
                        width: '100%',
                        height: 200
    
                    }}
                        resizeMode='stretch'
    
                        source={require('../../../Assets/Jeem.jpg')} />
                </View>
                <Animatable.View animation="fadeInUpBig" style={styles.loginScreenFooter}>
                
                <TextInput
                    onChangeText={value => {
                        if (isNaN(value)) {
                            return;
                        }
                        if (value.length > 6) {
                            return;
                        }
                        let val =
                            value + '------'.substr(0, 6 - value.length);
                        let a = [...val];
                        setOtpVal(a);
                        setOtp(value);
                    }}
                    style={{ height: 0 }}
                    autoFocus = {true}
                />
                <View style={styles.otpBoxesContainer}>
                    {[0, 1, 2, 3, 4, 5].map((item, index) => (
                        <Text style={styles.otpBox} key={index}>
                            {otp[item]}
                        </Text>
                    ))}
                </View>
    
                        <TouchableOpacity onPress={() => { submit() }
                        }>
                            <View style={styles.button}>
                                <LinearGradient
                                    colors={[constants.themeColor, constants.themeColor]}
                                    style={styles.btnSignIn}
                                >
                                    <Text style={styles.btnText}>
                                      Varify
             </Text>
                                </LinearGradient>
                            </View>
    
                        </TouchableOpacity>
    
                        
                </Animatable.View>
    
    
            </View>
           )
}
export default OTPViewController
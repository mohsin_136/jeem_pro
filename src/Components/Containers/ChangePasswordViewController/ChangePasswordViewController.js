import * as React from 'react';
import { Text, View, TextInput, Button, TouchableOpacity, StatusBar, Image } from 'react-native'
import * as Animatable from 'react-native-animatable'
import LinearGradient from 'react-native-linear-gradient'
import Feather from 'react-native-vector-icons/Feather'

import styles from '../../../Styles/styles'
import constants from '../../../Constants/Constants'
const ChangePasswordViewController = ({ navigation }) => {
    const [data, setData] = React.useState({
        oldPass: '',
        secureEntryOldPass: true,
        newPass: '',
        secureEntryNewPass: true,
    });

    const updateSecureTextEntryOldPass = (val) => {
        setData({
            ...data,
            secureEntryOldPass: !data.secureEntryOldPass
        })
    }
    const updateSecureTextEntryNewPass = (val) => {
        setData({
            ...data,
            secureEntryNewPass: !data.secureEntryNewPass
        })
    }

    const handleOldPassInput = (text) => {
        setData({
            ...data,
            oldPass: text

        })
    }
    const handleNewPassInput = (text) => {
        setData({
            ...data,
            newPass: text

        })
    }
    const submit = () => {
       navigation.navigate("Login")
    }
    return (
        <View style={styles.loginScreenContainer}>
            <StatusBar backgroundColor="#ff2241" barStyle="light-content" />
            <View style={styles.loginScreenHeader}>
                <Image style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    width: '100%',
                    height: 200

                }}
                    resizeMode='stretch'

                    source={require('../../../Assets/Jeem.jpg')} />
            </View>
            <Animatable.View animation="fadeInUpBig" style={styles.loginScreenFooter}>
            
            <Text style={[styles.text_footer, { marginTop: 35 }]}>
                    Password
    </Text>
                <View style={styles.action}>
                    <Feather
                        name="lock"
                        color="#05375a"
                        size={20}
                    />
                    <TextInput
                        placeholder='Password'
                        style={styles.textInput}
                        autoCapitalize='none'
                        secureTextEntry={data.secureEntryOldPass ? true : false}
                        onChangeText={(val) => handleOldPassInput(val)}
                    />
                    <TouchableOpacity onPress={updateSecureTextEntryOldPass}>
                        {data.secureEntryOldPass ?
                            <Feather
                                name="eye-off"
                                color='grey'
                                size={20}
                            />
                            :
                            <Feather
                                name="eye"
                                color='grey'
                                size={20}
                            />
                        }
                    </TouchableOpacity>
                </View>


                <Text style={[styles.text_footer, { marginTop: 35 }]}>
                  New  Password
    </Text>
                <View style={styles.action}>
                    <Feather
                        name="lock"
                        color="#05375a"
                        size={20}
                    />
                    <TextInput
                        placeholder='Password'
                        style={styles.textInput}
                        autoCapitalize='none'
                        secureTextEntry={data.secureEntryNewPass ? true : false}
                        onChangeText={(val) => handleNewPassInput(val)}
                    />
                    <TouchableOpacity onPress={updateSecureTextEntryNewPass}>
                        {data.secureEntryNewPass ?
                            <Feather
                                name="eye-off"
                                color='grey'
                                size={20}
                            />
                            :
                            <Feather
                                name="eye"
                                color='grey'
                                size={20}
                            />
                        }
                    </TouchableOpacity>
                </View>

                   







                    <TouchableOpacity onPress={() => { submit() }
                    }>
                        <View style={styles.button}>
                            <LinearGradient
                                colors={[constants.themeColor, constants.themeColor]}
                                style={styles.btnSignIn}
                            >
                                <Text style={styles.btnText}>
                                  Change Password
         </Text>
                            </LinearGradient>
                        </View>

                    </TouchableOpacity>

                    
            </Animatable.View>


        </View>
    )

}
export default ChangePasswordViewController
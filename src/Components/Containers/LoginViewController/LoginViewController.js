import * as React from 'react';
import { Text, View, TextInput, Button, TouchableOpacity, StatusBar, Image } from 'react-native'
import * as Animatable from 'react-native-animatable'
import LinearGradient from 'react-native-linear-gradient'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Feather from 'react-native-vector-icons/Feather'
// import {
//     GoogleSignin,
//     GoogleSigninButton,
//     statusCodes,
// } from '@react-native-community/google-signin';
import styles from '../../../Styles/styles'
import constants from '../../../Constants/Constants'
import { ScrollView } from 'react-native-gesture-handler';
const LoginViewController = ({ navigation }) => {
    const [data, setData] = React.useState({
        email: '',
        password: '',
        check_textInputChange: false,
        secureTextEntry: true
    });
    const [user, setUser] = React.useState([])


    // React.useEffect(() => {
    //     GoogleSignin.configure({
    //         scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
    //         webClientId: '385157613828-btff84aorc6cbg87sisn8od362o7kuj8.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
    //         iosClientId: '385157613828-va57le8tmhr5svmlcfgoci6mgdv3hs29.apps.googleusercontent.com', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    //     });
    // }, []);


  
//    const signIn = async () => {
//        console.log("INSIDE SIGN IN")
//         try {
//             await GoogleSignin.hasPlayServices();
//             const userInfo = await GoogleSignin.signIn();
//            setUser(userInfo)
//         } catch (error) {
//             if (error.code === statusCodes.SIGN_IN_CANCELLED) {
//                 console.log("Canceled")
//                 // user cancelled the login flow
//             } else if (error.code === statusCodes.IN_PROGRESS) {
//                 console.log("in progress already")
//                 // operation (e.g. sign in) is in progress already
//             } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
//                 // play services not available or outdated
//                 console.log("PlayService unavaiable")
//             } else {
//                 console.log("Error", error)
//                 // some other error happened
//             }
//         }
//     };

   

    const textInputChange = (val) => {
        if (val.length !== 0) {
            setData({
                ...data,
                email: val,
                check_textInputChange: true
            })

        }
        else {
            setData({
                ...data,
                email: val,
                check_textInputChange: false
            })
        }
    }
    const handlePasswordChange = (val) => {

        setData({
            ...data,
            password: val
        })
    }
    const updateSecureTextEntry = (val) => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        })
    }
    const loginHandle = (userName, pass) => {
        navigation.navigate('Home')
    }
    const skipHandle = (userName, pass) => {
        navigation.navigate('Home')
    }
    const signUp = () => {
        navigation.navigate('SignUp')
    }
    const handleForgotPass = () => {
        navigation.navigate("VarifyPhone")
    }

    return (
        <View style={styles.loginScreenContainer}>
            <StatusBar backgroundColor="#ff2241" barStyle="light-content" />
            <View style={styles.loginScreenHeader}>
                <Image style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    width: '100%',
                    height: 200

                }}
                    resizeMode='stretch'

                    source={require('../../../Assets/Jeem.jpg')} />
                    <TouchableOpacity onPress = {()=> skipHandle()} style={{
                            position: 'absolute',
                            left: 10,
                            right: 0,
                            top: 50,
                            bottom: 0
                        }}  >
                            <Text style={{color:'white', fontSize:18, fontWeight:'bold'}}> SKIP </Text>
                        </TouchableOpacity>
            </View>
            <Animatable.View animation="fadeInUpBig" style={styles.loginScreenFooter}>
                <ScrollView showsVerticalScrollIndicator={false}>
                <Text style={styles.text_footer}>
                    User Name
    </Text>

                <View style={styles.action}>
                    <FontAwesome
                        name="user-o"
                        color="#05375a"
                        size={20}
                    />
                    <TextInput
                        placeholder='Username'
                        style={styles.textInput}
                        autoCapitalize='none'
                        onChangeText={(val) => textInputChange(val)}
                    />
                    {
                        data.check_textInputChange ?
                            <Animatable.View

                                animation="bounceIn"
                            >
                                <Feather
                                    name="check-circle"
                                    color='green'
                                    size={20}
                                />
                            </Animatable.View>
                            : null
                    }
                </View>
                <Text style={[styles.text_footer, { marginTop: 35 }]}>
                    Password
    </Text>
                <View style={styles.action}>
                    <Feather
                        name="lock"
                        color="#05375a"
                        size={20}
                    />
                    <TextInput
                        placeholder='Password'
                        style={styles.textInput}
                        autoCapitalize='none'
                        secureTextEntry={data.secureTextEntry ? true : false}
                        onChangeText={(val) => handlePasswordChange(val)}
                    />
                    <TouchableOpacity onPress={updateSecureTextEntry}>
                        {data.secureTextEntry ?
                            <Feather
                                name="eye-off"
                                color='grey'
                                size={20}
                            />
                            :
                            <Feather
                                name="eye"
                                color='grey'
                                size={20}
                            />
                        }
                    </TouchableOpacity>
                </View>



                <TouchableOpacity onPress={() => { loginHandle(data.email, data.password) }
                }>
                    <View style={styles.button}>
                        <LinearGradient
                            colors={[constants.themeColor, constants.themeColor]}
                            style={styles.btnSignIn}
                        >
                            <Text style={styles.btnText}>
                                Sign In
         </Text>


                        </LinearGradient>
                    </View>

                </TouchableOpacity>

{/* <View style={{flexDirection:'row', justifyContent:'center', marginTop: 20}}>
                <GoogleSigninButton
                    style={{ width: 200, height: 55 }}
                    size={GoogleSigninButton.Size.Wide}
                    color={GoogleSigninButton.Color.Dark}
                    onPress={() => signIn()}
                    disabled={false} 
                    />
                    

</View> */}


                <TouchableOpacity onPress={() => { signUp() }
                }>
                    <View style={styles.button}>

                        <Text style={{ color: 'black' }}>
                            Dont have an Account? Sign Up
         </Text>



                    </View>

                </TouchableOpacity>

                <TouchableOpacity onPress={() => { handleForgotPass() }
                }>
                    <View style={styles.button}>

                        <Text style={{ color: 'black' }}>
                           Forgot Password?
         </Text>



                    </View>

                </TouchableOpacity>


                { user ? 
                
            <Text> {user} </Text>
            : null
            }

</ScrollView>
            </Animatable.View>


        </View>
    )
}
export default LoginViewController
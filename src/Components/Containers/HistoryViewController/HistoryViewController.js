import React, { Component } from 'react';
import { Text, View, FlatList, TouchableOpacity, StatusBar, StyleSheet, Dimensions, Image } from 'react-native'
import {Badge, Title, Paragraph} from 'react-native-paper'
const windowWidth = Dimensions.get('window').width;

const HistoryViewController = ({ navigation }) => {
    const [data, setData] = React.useState({
        dataSource: [
        { key:'1', name: 'Yesterday', image:'https://images3.alphacoders.com/823/thumb-1920-82317.jpg' , discription: 'Car Wash'},
        { key:'2', name: '28 October 2020', image:'https://images.pexels.com/photos/36717/amazing-animal-beautiful-beautifull.jpg?auto=compress&cs=tinysrgb&dpr=1&w=500' , discription: 'Car Wash'},
        { key:'3', name: '1 September 2020', image:'https://c4.wallpaperflare.com/wallpaper/500/442/354/outrun-vaporwave-hd-wallpaper-thumb.jpg' , discription: 'Car Wash'},
        
        ],

    });
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
            <StatusBar backgroundColor="#ff2241" barStyle="light-content" />
            <FlatList
            showsVerticalScrollIndicator = {false}
                style={{ marginTop: 40 }}
                data={data.dataSource}
                renderItem={({ item }) => (
                    <View style={{ justifyContent: 'flex-end', alignItems: 'center', backgroundColor: '#FFFFFF50', height: 300, width: windowWidth - 30, marginTop: 30, }}>
                        <Image style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            bottom: 0,
                            right: 0,
                            width: '100%',
                            height: 200,
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10
        
        
                        }}
                            resizeMode='stretch'
                            source = {{ uri: item.image }}
                      />
        
        <View style={{ backgroundColor: 'white', height: 100, width: '100%', borderRadius: 10 }}>
                           <Title style={{paddingTop:10, paddingLeft:15}}> {item.name} </Title>
                           <Paragraph style={{ paddingLeft:15}}> {item.discription} </Paragraph>
                        </View>
                    </View>
                   
        
                )}
            />

        </View>
    )
}

export default HistoryViewController
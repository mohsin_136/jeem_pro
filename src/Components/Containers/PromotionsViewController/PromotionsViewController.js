import React, { Component } from 'react';
import { Text, View, FlatList, TouchableOpacity, StatusBar, StyleSheet, Dimensions, Image, ScrollView } from 'react-native'
import { Badge, Title, Paragraph, Switch } from 'react-native-paper'
import LinearGradient from 'react-native-linear-gradient'
const windowWidth = Dimensions.get('window').width;
import styles from '../../../Styles/styles'
import constants from '../../../Constants/Constants'

const PromotionsViewController =  ({ navigation }) =>  {
    const [data, setData] = React.useState({
        dataSource: [
            {
                key: '1',
                name: '50% Discount',
                image: 'https://images3.alphacoders.com/823/thumb-1920-82317.jpg',
                discription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                customizations: [
                    {
                        on: true,
                        title: 'Interior AC and Vent Streaming'
                    },
                    {
                        on: false,
                        title: 'Interior Cleaning'

                    },
                    {
                        on: false,
                        title: 'Leather Cleaning'
                    },
                    {
                        on: true,
                        title: 'Engine Stream'

                    },
                ]
            },
            {
                key: '2',
                name: '60% Discount',
                image: 'https://images.pexels.com/photos/36717/amazing-animal-beautiful-beautifull.jpg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                discription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                customizations: [
                    {
                        on: false,
                        title: 'Interior AC and Vent Streaming'
                    },
                    {
                        on: false,
                        title: 'Interior Cleaning'

                    },
                    {
                        on: true,
                        title: 'Leather Cleaning'
                    },
                    {
                        on: true,
                        title: 'Engine Stream'

                    },
                ]
            },
            {
                key: '3',
                name: '70% Discount',
                image: 'https://c4.wallpaperflare.com/wallpaper/500/442/354/outrun-vaporwave-hd-wallpaper-thumb.jpg',
                discription: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
                customizations: [
                    {
                        on: true,
                        title: 'Interior AC and Vent Streaming'
                    },
                    {
                        on: true,
                        title: 'Interior Cleaning'

                    },
                    {
                        on: false,
                        title: 'Leather Cleaning'
                    },
                    {
                        on: false,
                        title: 'Engine Stream'

                    },
                ]
            },

        ],

    });
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
            <StatusBar backgroundColor="#ff2241" barStyle="light-content" />

            <FlatList
                showsVerticalScrollIndicator={false}
                style={{ marginTop: 40 }}
                data={data.dataSource}
                renderItem={({ item }) => (
            <TouchableOpacity
            onPress={() => {
                navigation.push('PromotionsDetail', {
                    itemImage: item.image,
                    itemName: item.name,
                    itemDes: item.discription,
                    itemCustomizations: item.customizations
                });
              }}
                >
            <View style={{ justifyContent: 'flex-end', alignItems: 'center', backgroundColor: '#FFFFFF50', height: 400, width: windowWidth - 30, marginTop: 30, marginBottom: 30}}>
                        <Image style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            bottom: 0,
                            right: 0,
                            width: '100%',
                            height: 200,
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10
                        }}
                            resizeMode='stretch'
                            source={{ uri: item.image }}
                        />
                        <View style={{ backgroundColor: 'white', width: '100%', borderRadius: 10, height: 200 }}>
                            <Title style={{ paddingTop: 5, paddingLeft: 15, fontFamily:'Montserrat-Bold' }}> {item.name} </Title>
                            <Title style={{ paddingTop: 3, fontSize: 12, paddingLeft: 15, fontFamily:"Montserrat-SemiBold" }}> About this package </Title>
                            <Paragraph style={{ paddingLeft: 15, fontSize: 10, fontFamily:"Montserrat-Regular" }}> {item.discription} </Paragraph>
                        </View>
                    </View>

                    </TouchableOpacity>
                )}
            />

        </View>
    )
}

export default PromotionsViewController
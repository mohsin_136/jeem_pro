import * as React from 'react';
import { Text, View, TextInput, Button, TouchableOpacity, StatusBar, ScrollView, Image } from 'react-native'
import * as Animatable from 'react-native-animatable'
import LinearGradient from 'react-native-linear-gradient'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Entypo from 'react-native-vector-icons/Entypo'
import styles from '../../../Styles/styles'
import constants from '../../../Constants/Constants'
import GetLocation from 'react-native-get-location'
import Geocoder from 'react-native-geocoding';
const GetInfoViewController = ({ navigation }) => {
    const [data, setData] = React.useState({
        vehicleNumber: '',
        vehicleModel: '',
        vehicleYear: '',
        vehicleType: '',
        vehicleBrand: '',
        description: '',
        availibilityDateFrom: '',
        avalibilityDateTo: '',
        availityTimeFrom: '',
        availityTimeTo: '',

    });


    // For Picking Date and TIme From
    const [isDatePickerVisible, setDatePickerVisibility] = React.useState(false);
    const [isTimePickerVisible, setTimePickerVisibility] = React.useState(false);

    // For Picking Date and TIme To
    const [isDatePickerVisibleTo, setDatePickerVisibilityTo] = React.useState(false);
    const [isTimePickerVisibleTo, setTimePickerVisibilityTo] = React.useState(false);


    // For Picking Date and TIme From
    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };
    const showTimePicker = () => {
        setTimePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const hideTimePicker = () => {
        setTimePickerVisibility(false);
    };

    // For Picking Date and TIme To
    const showDatePickerTo = () => {
        setDatePickerVisibilityTo(true);
    };
    const showTimePickerTo = () => {
        setTimePickerVisibilityTo(true);
    };

    const hideDatePickerTo = () => {
        setDatePickerVisibilityTo(false);
    };

    const hideTimePickerTo = () => {
        setTimePickerVisibilityTo(false);
    };

    // For Picking Date and TIme From

    const handleConfirm = (date) => {
        // console.warn("A date has been picked: ", date);
        hideDatePicker();
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();
        var yearSelected = year.toString()
        setData({
            ...data,
            availibilityDateFrom: day + "-" + month + "-" + yearSelected
        })
        showTimePicker();
    };
    const handleTimeConfirm = (time) => {
        //  console.warn("A time has been picked: ", time);
        let hours = time.getHours();
        let minutes = time.getMinutes();
        let seconds = time.getSeconds();
        setData({
            ...data,
            availityTimeFrom: `${hours}:${minutes}:${seconds}`
        })
        hideTimePicker();
    };


    // For Picking Date and TIme To
    const handleConfirmTo = (date) => {
        //   console.warn("A date has been picked: ", date);
        hideDatePicker();
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();
        var yearSelected = year.toString()
        setData({
            ...data,
            avalibilityDateTo: day + "-" + month + "-" + yearSelected
        })

        showTimePickerTo();
    };
    const handleTimeConfirmTo = (time) => {
        //  console.warn("A time has been picked: ", time);
        let hours = time.getHours();
        let minutes = time.getMinutes();
        let seconds = time.getSeconds();
        setData({
            ...data,
            availityTimeTo: `${hours}:${minutes}:${seconds}`
        })
        hideTimePickerTo();
    };


    const handleVehicleTypeInputChange = (val) => {

        setData({
            ...data,
            vehicleType: val,
        })
    }
    const handleVehicleModelInputChange = (val) => {

        setData({
            ...data,
            vehicleModel: val,
        })
    }
    const handleVehicleYearInputChange = (val) => {

        setData({
            ...data,
            vehicleYear: val,
        })
    }
    const handleVehicleBrandInputChange = (val) => {

        setData({
            ...data,
            vehicleBrand: val,
        })
    }

    const handleVehicleNumberInputChange = (val) => {

        setData({
            ...data,
            vehicleNumber: val,
            check_textInputChange: true
        })
    }
    const handleDescriptionChange = (val) => {

        setData({
            ...data,
            password: val
        })
    }

    const handleLocationPicker = () => {
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        })
            .then(location => {
              console.log(location)
              fillingAutoLocation(location.latitude, location.longitude)
            })
            .catch(error => {
                const { code, message } = error;
                console.warn(code, message);
            })
    }


    const fillingAutoLocation = (lat, lng) => {
        Geocoder.init("AIzaSyCt-KoTJNWNzbfFwp-I6fPDR7unSGspKNY");
        Geocoder.from(lat, lng)
        .then(json => {
        		var addressComponent = json.results[0].address_components[0];
            console.log(addressComponent);
        })
        .catch(error => console.warn(error));

    }
    const submit = () => {
        navigation.navigate('Login')
    }

    return (
        <View style={styles.loginScreenContainer}>
            <StatusBar backgroundColor="#ff2241" barStyle="light-content" />
            <Animatable.View animation="fadeInUpBig" style={styles.loginScreenFooter}>
                <ScrollView showsVerticalScrollIndicator={false}>

                    <Text style={[styles.text_footer, { marginTop: 35 }]}>
                        Vehicle Number
    </Text>

                    <View style={styles.action}>

                        <TextInput
                            placeholder='ABC-0000'
                            style={styles.textInput}
                            autoCapitalize='none'
                            onChangeText={(val) => handlePhoneNumberInput(val)}
                        />
                    </View>

                    <Text style={[styles.text_footer, { marginTop: 35 }]}>
                        Vehicle Model
    </Text>

                    <View style={styles.action}>

                        <TextInput
                            placeholder='Model No'
                            style={styles.textInput}
                            autoCapitalize='none'
                            onChangeText={(val) => handleVehicleModelInputChange(val)}
                        />
                    </View>

                    <Text style={[styles.text_footer, { marginTop: 35 }]}>
                        Vehicle Year
    </Text>

                    <View style={styles.action}>

                        <TextInput
                            placeholder='2020'
                            style={styles.textInput}
                            autoCapitalize='none'
                            onChangeText={(val) => handleVehicleYearInputChange(val)}
                        />
                    </View>


                    <Text style={styles.text_footer}>
                        Vehicle Type
    </Text>

                    <View style={styles.action}>
                        <TextInput
                            placeholder='Car'
                            style={styles.textInput}
                            autoCapitalize='none'
                            onChangeText={(val) => handleVehicleTypeInputChange(val)}
                        />
                    </View>

                    <Text style={[styles.text_footer, { marginTop: 35 }]}>
                        Vehicle Brand
    </Text>

                    <View style={styles.action}>

                        <TextInput
                            placeholder='Mecedeze'
                            style={styles.textInput}
                            autoCapitalize='none'
                            onChangeText={(val) => handleVehicleBrandInputChange(val)}
                        />
                    </View>


                    <Text style={[styles.text_footer, { marginTop: 35 }]}>
                        Availibility
    </Text>

                    <DateTimePickerModal
                        isVisible={isDatePickerVisible}
                        mode="date"
                        onConfirm={handleConfirm}
                        onCancel={hideDatePicker}
                    />
                    <DateTimePickerModal
                        isVisible={isTimePickerVisible}
                        mode="time"
                        onConfirm={handleTimeConfirm}
                        onCancel={hideTimePicker}
                    />
                    <DateTimePickerModal
                        isVisible={isDatePickerVisibleTo}
                        mode="date"
                        onConfirm={handleConfirmTo}
                        onCancel={hideDatePickerTo}
                    />
                    <DateTimePickerModal
                        isVisible={isTimePickerVisibleTo}
                        mode="time"
                        onConfirm={handleTimeConfirmTo}
                        onCancel={hideTimePickerTo}
                    />

                    <View style={{
                        flexDirection: 'row',
                        marginTop: 10, marginLeft: 20,
                        paddingBottom: 5
                    }}>
                        <TouchableOpacity onPress={showDatePicker}>
                            <View style={[styles.action, { width: 130 }]}>

                                <TextInput
                                    placeholder='Pick a date'
                                    style={styles.textInput}
                                    autoCapitalize='none'
                                    editable={false}
                                    defaultValue={data.availibilityDateFrom + "  " + data.availityTimeFrom}

                                />
                            </View>

                        </TouchableOpacity>
                        <Text > to </Text>

                        <TouchableOpacity onPress={showDatePickerTo}>
                            <View style={[styles.action, { width: 130 }]}>

                                <TextInput
                                    placeholder='Pick a date'
                                    style={[styles.textInput,]}
                                    autoCapitalize='none'
                                    editable={false}
                                    defaultValue={data.avalibilityDateTo + "  " + data.availityTimeTo}

                                />
                            </View>

                        </TouchableOpacity>

                    </View>



                    <Text style={[styles.text_footer, { marginTop: 35 }]}>
                        Address
    </Text>
                    <View style={styles.action}>
                        <TextInput
                            placeholder='Address'
                            style={styles.textInput}
                            autoCapitalize='none'
                            onChangeText={(val) => handleDescriptionChange(val)}
                        />

                        <Entypo name = "location-pin" size = {25} color ={"red"} onPress={() =>handleLocationPicker()} />

                    </View>

                    <TouchableOpacity onPress={() => { submit() }
                    }>
                        <View style={styles.button}>
                            <LinearGradient
                                colors={[constants.themeColor, constants.themeColor]}
                                style={styles.btnSignIn}
                            >
                                <Text style={styles.btnText}>
                                    Submit
         </Text>
                            </LinearGradient>
                        </View>

                    </TouchableOpacity>
                </ScrollView>

            </Animatable.View>


        </View>
    )

}
export default GetInfoViewController
import * as React from 'react';
import { Text, View, TextInput, Button, TouchableOpacity, StatusBar, ScrollView, Image } from 'react-native'
import * as Animatable from 'react-native-animatable'
import LinearGradient from 'react-native-linear-gradient'
import PhoneInput from 'react-native-phone-number-input';

import styles from '../../../Styles/styles'
import constants from '../../../Constants/Constants'
const GetPhoneNumberViewController = ({ navigation }) => {
    const [data, setData] = React.useState({
        phoneNumber: '',
    });
    const handlePhoneNumberInput = (text) => {
        setData({
            ...data,
            phoneNumber: text

        })
    }
    const submit = () => {
        navigation.navigate('OTP')
    }
    return (
        <View style={styles.loginScreenContainer}>
            <StatusBar backgroundColor="#ff2241" barStyle="light-content" />
            <View style={styles.loginScreenHeader}>
                <Image style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    width: '100%',
                    height: 200

                }}
                    resizeMode='stretch'

                    source={require('../../../Assets/Jeem.jpg')} />
            </View>
            <Animatable.View animation="fadeInUpBig" style={styles.loginScreenFooter}>
            
                    <Text style={[styles.text_footer, { marginTop: 35 }]}>
                        Phone
    </Text>
                    <View style={styles.action}>
                        <PhoneInput
                            containerStyle={{ height: 50, backgroundColor: 'white' }}
                            textInputStyle={{ backgroundColor: 'white', height: 50 }}
                            codeTextStyle={{ backgroundColor: 'white' }}
                            flagButtonStyle={{ height: 50 }}
                            defaultValue={data.phoneNumber}
                            defaultCode='SA'
                            onChangeText={(text) => handlePhoneNumberInput(text)}
                        />

                    </View>


                    <TouchableOpacity onPress={() => { submit() }
                    }>
                        <View style={styles.button}>
                            <LinearGradient
                                colors={[constants.themeColor, constants.themeColor]}
                                style={styles.btnSignIn}
                            >
                                <Text style={styles.btnText}>
                                   Send OTP
         </Text>
                            </LinearGradient>
                        </View>

                    </TouchableOpacity>

                    
            </Animatable.View>


        </View>
    )

}
export default GetPhoneNumberViewController
import React, { Component } from 'react';
import {
    StyleSheet,
    Platform
} from 'react-native';
import constants from '../Constants/Constants'
const styles = StyleSheet.create({

    loginScreenContainer: {
        flex: 1,
        backgroundColor: constants.themeColor
    },
    loginScreenHeader: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // paddingHorizontal: 20,
        paddingBottom: 50,
        alignItems: 'stretch',
        position: 'relative'
    },
    loginScreenFooter: {
        flex: 3,
        backgroundColor: constants.whiteColor,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    homeScreenHeader: {
        flex: 2,
        justifyContent: 'center',
        paddingHorizontal: 20,
        paddingBottom: 50,
        alignItems: 'center'
    },
    title_header: {
        color: constants.whiteColor,
        fontWeight: 'bold',
        fontSize: 30
    },
    text_footer: {
        color: '#05375a',
        fontSize: 18,
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    bigTextBox: {
        borderWidth: 1,
        borderColor: '#f2f2f2',
        paddingBottom: 5,
        height: 120,
        marginTop: 10
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
    bigTextBox: {
        borderWidth: 1,
        borderColor: '#f2f2f2',
        paddingBottom: 5,
        height: 120,
        marginTop: 10
    },
    otpTextBox: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#f2f2f2',
        height: 70,
        width: 30,
        margin: 10
    },

    button: {
        alignItems: 'center',
        marginTop: 50,


    },
    pkgBtn: {
        alignItems: 'center',
        marginTop: 5,
        marginLeft:15,
        marginRight:15


    },
    signUpBtn: {
        marginBottom: 20


    },
    btnSignIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    btnBuyPakage: {
        width: '100%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
       
    },
    btnText: {
        fontWeight: 'bold',
        fontSize: 18,
        color: constants.whiteColor
    },
    btnTextBuyPakage: {
        fontWeight: 'bold',
        fontSize: 12,
        color: constants.whiteColor
    },
    buttona: {
        marginTop: 20,
        height: 50,
        width: 100,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 5,
    },
    message: {
        borderWidth: 1,
        borderRadius: 5,
        padding: 20,
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },

    otpBoxesContainer: {
        flexDirection: 'row'
    },
    otpBox: {
        padding: 10,
        marginRight: 10,
        borderWidth: 1,
        borderColor:'#D3D3D3',
        height: 45,
        width: 45,
        textAlign: 'center'
    }
})

export default styles